from locust import TaskSet, task
from realbrowserlocusts import ChromeLocust, PhantomJSLocust
import logging
logging.basicConfig(level=logging.DEBUG)

HOST_WEBSITE = 'https://www.google.com'

class UserBehavior(TaskSet):
    def __init__(self, parent):
        super().__init__(parent)

    def on_start(self):
        logging.info('starting user')
        self.client.driver.set_window_size(1024, 768)
        self.client.driver.maximize_window()

    def deal(self):
        try:
            self.client.get(HOST_WEBSITE)
        except Exception as e:
            logging.error(e)

    def on_stop(self):
        logging.info('stopping user')
        self.client.driver.quit()

    @task(1)
    def run_deal(self):
        try:
            self.client.timed_event_for_locust("DEAL", "deal", self.deal)
        except Exception as e:
            logging.error(e)

class LocustUser(ChromeLocust):
    host = HOST_WEBSITE
    timeout = 30
    min_wait = 100
    max_wait = 1000
    screen_width = 1440
    screen_height = 1086
    task_set = UserBehavior
